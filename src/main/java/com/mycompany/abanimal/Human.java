/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.abanimal;

/**
 *
 * @author 66955
 */
public class Human extends Landanimal {

    private String nickname;

    public Human(String nickname) {
        super("Human", 2);
        this.nickname = nickname;
    }

    public void walk() {
        System.out.println("Human : " + nickname + " walk");
    }

    @Override
    public void run() {
        System.out.println("Human : " + nickname + " run");
    }

    @Override
    public void eat() {
        System.out.println("Human : " + nickname + " eat");
    }

    @Override
    public void speak() {
        System.out.println("Human : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Human : " + nickname + " sleep");
    }

}
