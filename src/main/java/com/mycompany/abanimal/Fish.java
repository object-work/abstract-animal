/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.abanimal;

/**
 *
 * @author 66955
 */
public class Fish extends Aquaticanimal{
    private String nickname;

    public Fish(String nickname) {
        super("Fish");
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        System.out.println("Fish : " + nickname + " eat");
    }

    @Override
    public void speak() {
        System.out.println("Fish : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Fish : " + nickname + " sleep");
    }
    
    @Override
    public void swim() {
        System.out.println("Fish : " + nickname + " swim");
    }

}
