/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.abanimal;

/**
 *
 * @author 66955
 */
public class Testanimal {
    public static void main (String[] args){
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        
         
        System.out.println("h1 is Animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is Land Animal ? " + (h1 instanceof Landanimal));
        
        Animal a1 = h1;
        System.out.println("a1 is LandAnimal ? " + (a1 instanceof Landanimal));
        System.out.println("a1 is Repitle Animal? " + (a1 instanceof Reptile));
        System.out.println("a1 is Aquaticanimal Animal? " + (a1 instanceof Aquaticanimal));
        System.out.println("a1 is Poultry Animal? " + (a1 instanceof Poultry));
        
        Cat c1 = new Cat("Dom");
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        
        System.out.println("c1 is Animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is Land Animal ? " + (c1 instanceof Landanimal));
        
        Animal a2 = c1;
        System.out.println("a2 is LandAnimal ? " + (a2 instanceof Landanimal));
        System.out.println("a2 is Repitle Animal? " + (a2 instanceof Reptile));
        System.out.println("a2 is Aquaticanimal Animal? " + (a2 instanceof Aquaticanimal));
        System.out.println("a2 is Poultry Animal? " + (a2 instanceof Poultry));
        
        Dog d1 = new Dog("Tom");
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        
        System.out.println("d1 is Animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is Land Animal ? " + (d1 instanceof Landanimal));
        
        Animal a3 = d1;
        System.out.println("a3 is LandAnimal ? " + (a3 instanceof Landanimal));
        System.out.println("a3 is Repitle Animal? " + (a3 instanceof Reptile));
        System.out.println("a3 is Aquaticanimal Animal? " + (a3 instanceof Aquaticanimal));
        System.out.println("a3 is Poultry Animal? " + (a3 instanceof Poultry));
        
        Snake s1 = new Snake("Six");
        s1.eat();
        s1.speak();
        s1.sleep();
        
        System.out.println("s1 is Animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is Repitle Animal? " + (s1 instanceof Reptile));
        
        Animal a4 = s1;
        System.out.println("a4 is Repitle Animal? " + (a4 instanceof Reptile));
        System.out.println("a4 is Land Animal ? " + (a4 instanceof Landanimal));
        System.out.println("a4 is Aquaticanimal Animal? " + (a4 instanceof Aquaticanimal));
        System.out.println("a4 is Poultry Animal? " + (a4 instanceof Poultry));
        
        Crocodile co1 = new Crocodile("Ka");
        co1.eat();
        co1.walk();
        co1.speak();
        co1.sleep();
        
        System.out.println("co1 is Animal ? " + (co1 instanceof Animal));
        System.out.println("co1 is Repitle Animal? " + (co1 instanceof Reptile));
        
        Animal a5 = co1;
        System.out.println("a5 is Repitle Animal? " + (a5 instanceof Reptile));
        System.out.println("a5 is Land Animal ? " + (a5 instanceof Landanimal));
        System.out.println("a5 is Aquaticanimal Animal? " + (a5 instanceof Aquaticanimal));
        System.out.println("a5 is Poultry Animal? " + (a5 instanceof Poultry));
        
        Fish f1 = new Fish("Five");
        f1.eat();
        f1.speak();
        f1.sleep();
        f1.swim();
        
        System.out.println("f1 is Animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is Aquaticanimal Animal? " + (f1 instanceof Aquaticanimal));
        
        Animal a6 = f1;
        System.out.println("a6 is Aquaticanimal Animal? " + (a6 instanceof Aquaticanimal));
        System.out.println("a6 is Repitle Animal? " + (a6 instanceof Reptile));
        System.out.println("a6 is Land Animal ? " + (a6 instanceof Landanimal));
        System.out.println("a6 is Poultry Animal? " + (a6 instanceof Poultry));
        
        Crab ca1 = new Crab("Jak");
        ca1.eat();
        ca1.walk();
        ca1.speak();
        ca1.sleep();
        ca1.swim();
        
        System.out.println("ca1 is Animal ? " + (ca1 instanceof Animal));
        System.out.println("ca1 is Aquaticanimal Animal? " + (ca1 instanceof Aquaticanimal));
        
        Animal a7 = ca1;
        System.out.println("a7 is Aquaticanimal Animal? " + (a7 instanceof Aquaticanimal));
        System.out.println("a7 is Repitle Animal? " + (a7 instanceof Reptile));
        System.out.println("a7 is Land Animal ? " + (a7 instanceof Landanimal));
        System.out.println("a7 is Poultry Animal? " + (a7 instanceof Poultry));
        
        Bat ba1 = new Bat("Jak");
        ba1.eat();
        ba1.walk();
        ba1.speak();
        ba1.sleep();
        ba1.fly();
        
        System.out.println("ba1 is Animal ? " + (ba1 instanceof Animal));
        System.out.println("ba1 is Poultry Animal? " + (ba1 instanceof Poultry));
        
        Animal a8 = ba1;
        System.out.println("a8 is Aquaticanimal Animal? " + (a8 instanceof Aquaticanimal));
        System.out.println("a8 is Repitle Animal? " + (a8 instanceof Reptile));
        System.out.println("a8 is Land Animal ? " + (a8 instanceof Landanimal));
        System.out.println("a8 is Poultry Animal? " + (a8 instanceof Poultry));
        
        Bird b1 = new Bird("Angel");
        b1.eat();
        b1.walk();
        b1.speak();
        b1.sleep();
        b1.fly();
        
        System.out.println("b1 is Animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is Poultry Animal? " + (b1 instanceof Poultry));
        
        Animal a9 = b1;
        System.out.println("a9 is Aquaticanimal Animal? " + (a9 instanceof Aquaticanimal));
        System.out.println("a9 is Repitle Animal? " + (a9 instanceof Reptile));
        System.out.println("a9 is Land Animal ? " + (a9 instanceof Landanimal));
        System.out.println("a9 is Poultry Animal? " + (a9 instanceof Poultry));
         
    }
}
