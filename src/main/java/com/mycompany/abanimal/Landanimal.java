/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.abanimal;

/**
 *
 * @author 66955
 */
public abstract class Landanimal extends Animal{
    
    public Landanimal(String name,int numberOfLegs){
        super(name,numberOfLegs);
    }
    
    public abstract void run();

}
